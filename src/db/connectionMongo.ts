import  mongoose   from "mongoose";
import dotenv from './../config/enviroments';

const db_connection = async() =>{
  try {
    const connection = await mongoose.connect(
        dotenv.db_url,
        {
          useNewUrlParser: true,
          useFindAndModify: false,
          useUnifiedTopology: true
        }
      );

      if(connection)
        console.log('connection db is ON')
    return connection;
  } catch (error) {
      
  }
      
}


export default db_connection();
  