import { tokenModel } from "./schema/token";




export const deleteToken = async(token:string):Promise<Boolean>=>{
    await tokenModel.create({token});
    return true;
}


export const validateIfTokenIsNotValid = async(token:string):Promise<Boolean>=>{
    const tokenValue = await tokenModel.findOne({token});

    return tokenValue ?  true : false;
   
}