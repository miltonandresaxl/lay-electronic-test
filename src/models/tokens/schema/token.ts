import { model, Schema } from "mongoose";
import { v4 as uuid} from 'uuid';


const TokenSchema = new Schema({
    token:String
}, {timestamps:true});


export const tokenModel = model('tokens', TokenSchema);