import { UserDtoCreate, UserDtoUpdate } from "./dto/user";
import { InUser } from "./interface/InUser"
import { userModel } from "./schemas/user"


export const createUser = async(payload:UserDtoCreate) :Promise<InUser> => {
    return await userModel.create(payload);
}

export const getUserByUserName = async(userName:string):Promise<InUser> => {
    return await userModel.findOne({userName});
}


export const getUserByToken = async(id:string):Promise<InUser>=>{
    return await userModel.findOne({id});
}


export const activeUser = async(id:string):Promise<InUser> =>{
    await userModel.updateOne({
        id
    }, {$set:{isActive:true}});

    return await userModel.findOne({
        id
    });
}


export const deleteUser = async (id:string):Promise<Boolean>=>{
    await userModel.deleteOne({id})
    return true;
}


export const getUserActive = async(id:string):Promise<InUser>=>{
    return await userModel.findOne({$and:[{id}, {isActive:true}]});
}


export const updateUser = async(id:string ,payload:UserDtoUpdate) :Promise<InUser> => {
     await userModel.updateOne({id}, {$set:{fullName:payload.fullName}});
     return await userModel.findOne({
        id
    }); 
}