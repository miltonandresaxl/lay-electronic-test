import { model, Schema } from "mongoose";
import { v4 as uuid} from 'uuid';


const UserSchema = new Schema({
    id:{
        type:String,
        default:uuid().substr(0, 8).toLocaleUpperCase(),
        unique:true
    },
    fullName:{
        type:String,
        required:true
    },
    userName:{
        type:String,
        required:true,
        unique:true
    },
    password:{
        type:String,
        required:true
    },
    isActive:{
        type:Boolean,
        default:false
    }
}, {timestamps:true});


export const userModel = model('users', UserSchema);