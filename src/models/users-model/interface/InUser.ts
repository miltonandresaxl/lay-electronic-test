import {  Document } from 'mongoose'
 
export class InUser  extends Document {
    readonly id:string;
    readonly fullName:string;
    readonly userName:string;
    readonly password:string;
    readonly isActive:boolean;
}