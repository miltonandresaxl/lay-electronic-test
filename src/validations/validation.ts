
import { body } from 'express-validator';



export const validateUser = [ 
    
    ...[
        body('fullName', 'fullName is required. Please verify and try again').exists().isLength({min:4}).isString()
    ],
    ...[
        body('userName', 'userName is required. Please verify and try again').exists().isLength({min:6}).isString()
    ],
    ...[
         body('password', 'password is required. Please verify and try again').exists().isLength({min:8}).isString()
    ]
]



export const ValidateCreateSession = [ 
    
    ...[
        body('userName', 'userName is required. Please verify and try again').exists().isLength({min:6}).isString()
    ],
    ...[
         body('password', 'password is required. Please verify and try again').exists().isLength({min:8}).isString()
    ]
]


export const  validateUpdateUser = [ 
    
    ...[
        body('fullName', 'fullName is required. Please verify and try again').exists().isLength({min:10}).isString()
    ]
]