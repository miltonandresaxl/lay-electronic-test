import { NextFunction, Request, Response } from "express";
import { decodeToken } from "../helpers";
import { validateIfTokenIsNotValid } from "../models/tokens/token-model.service";
import { getUserByToken } from "../models/users-model/use-model.service";

export const authMiddleware = async(req:Request, res:Response, next:NextFunction)=>{
    try {
        const header = req.headers.authorization;

        if(!header || !(header as String).split(" ")[1]) throw Error('session token is required');

        const validateToken = await validateIfTokenIsNotValid((header as String).split(" ")[1]);

        if(validateToken) return  res.status(401).json({
            status:401,
            message:'Token invalid!'
        })


        const token:any = decodeToken((header as String).split(" ")[1]);
        const user = await getUserByToken(token.user.id) ;
        if(!user) return  res.status(404).json({
            status:404,
            message:'User not found'
        })

        res.locals.user= user
        next()
    } catch ({message}) {
        return  res.status(401).json({
            status:401,
            message:message
        })
    }
}