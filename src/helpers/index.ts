

import jwt from 'jsonwebtoken';


import { hashSync,compareSync, genSaltSync } from 'bcrypt';

import dotenv from './../config/enviroments';


export const encodeToken = (id:string)=> jwt.sign({user:{id}}, dotenv.tokenSALT);


export const decodeToken = (token:string)=> jwt.verify(token, dotenv.tokenSALT);



export const encodeBcrypt = (text:string)=> hashSync(text, genSaltSync(Number(dotenv.bcryptSALT)));


export const validateBcrypt = (text:string, validate:string) => compareSync(text, validate);