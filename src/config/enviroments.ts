
import dotenv from 'dotenv';

const dotenvLoad = ()=>{
    const envPath = 'src/env/.env';
    dotenv.config({ path: envPath });



    return {
        port:process.env.port,
        db_url:`mongodb+srv://${process.env.db_user}:${process.env.db_password}@${process.env.db_url}/${process.env.db_database}?retryWrites=true&w=majority`,
        tokenSALT:process.env.tokenSALT || '',
        bcryptSALT:process.env.bcryptSALT || '',
        mqtt_url:`mqtt://${process.env.mqtt_url}:${process.env.mqtt_port}`, //'mqtt.lyaelectronic.com',
        mqtt_topic:`${process.env.mqtt_topic}`
    }
}


export default dotenvLoad();