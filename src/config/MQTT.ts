import { connect } from 'mqtt';

import dotenv from './../config/enviroments';


export const connectionAndSendMessage = (message:string, user_id:string)=>{
    try {
        const client = connect(dotenv.mqtt_url, {protocolId: 'MQIsdp', protocolVersion: 3, connectTimeout:1000});
        client.on('connect', ()=>{
                client.subscribe(dotenv.mqtt_topic, (err)=> {
                    if(err) console.log(err)
                });
        })
        const string:string = JSON.stringify({message, user_id}) 
        client.publish(dotenv.mqtt_topic, string);  

        return message;
    } catch ({message}) {
        console.log(message)
    }
   
  
}
