import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import swaggerUEX from 'swagger-ui-express';
import swaggerDoc from 'swagger-jsdoc';

import dotenv from './config/enviroments';
import db_connection from './db/connectionMongo';
import index from './routes/index.routes';
import { options } from './config/swagger';


//Initializer
const app = express();

dotenv;
db_connection;


//middleware
app
    .use(cors())
    .use(morgan('dev'))
    .use(express.json())

//routes 
    .use(index)

//swagger 
    .use('/api/doc', swaggerUEX.serve, swaggerUEX.setup(swaggerDoc(options)));

export default app;