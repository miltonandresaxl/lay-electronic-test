import { Request, Response } from "express";
import { deleteSessionTokenService } from "./token.service";

export const deleteSessionTokenController  =async (req:Request, res:Response):Promise<Response>=>{
    const header = req.headers.authorization;
    return res.status(200).json({
            status:200,
            data:await deleteSessionTokenService((header as String).split(" ")[1])
        })
}