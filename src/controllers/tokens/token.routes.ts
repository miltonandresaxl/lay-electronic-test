import { Router } from "express";
import { authMiddleware } from "../../middleware/authMiddleware";
import { ValidateCreateSession } from "../../validations/validation";
import { getSessionUserController } from "../users/user.controller";
import { deleteSessionTokenController } from "./token.conmtroller";


const _routerToken = Router()



/**
 * 
 * @swagger
 * 
 * /authorization:
 * 
 *      post:
 *          summary: Returns session create
 *          requestBody:
 *              description: crete a new session
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 * 
 *                          properties:
 *                              userName:
 *                                  type: string
 *                              password:
 *                                  type: string
 *                          required:
 *                              - fullName
 *                              - userName
 *                              - password
 * 
 *          responses:
 *              200:
 *                  description: create a new session
 *                  content:
 *                      application/json:
 *                          schema:
 *                              type: object
 *                              properties:
 *                                      status: 
 *                                          type: integer
 *                                      token: 
*                                           type: string
 *                              
 *              400:
 *                  description: Bad request
 *                  content:
 *                      application/json:
 *                          schema:
 *                              $ref: '#/components/schemas/Error'
 *              403:
 *                  description: Forbidden
 *                  content:
 *                      application/json:
 *                          schema:
 *                              $ref: '#/components/schemas/Forbidden'
 *                  
 *          
 * 
 */


_routerToken.post ('/', [...ValidateCreateSession], getSessionUserController);


/**
 * @swagger
 * 
 *  /authorization:
 *      
 *      delete:
 *          
 *          summary: Returns token delete
 *          security:
 *               - bearerAuth: []
 *          responses:
 *              200:
 *                  description: token delete
 *                              
 *              401:
 *                $ref: '#/components/responses/UnauthorizedError'
 * 
 *              403:
 *                  description: Forbidden
 *                  content:
 *                      application/json:
 *                          schema:
 *                              $ref: '#/components/schemas/Forbidden'
 *                      
 *                      
 *         
 */
_routerToken.delete ('/', [authMiddleware, ...ValidateCreateSession], deleteSessionTokenController);



export default _routerToken;