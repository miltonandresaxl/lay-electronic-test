import { deleteToken } from "../../models/tokens/token-model.service";

export const deleteSessionTokenService = async(token:string):Promise<Boolean>=>{
    return await deleteToken(token);
}
