

import { JwtPayload } from "jsonwebtoken";
import {  encodeBcrypt, encodeToken, validateBcrypt } from "../../helpers";
import { deleteToken } from "../../models/tokens/token-model.service";
import { UserDtoCreate, UserDtoUpdate } from "../../models/users-model/dto/user"
import { InUser } from "../../models/users-model/interface/InUser"
import { activeUser, createUser, deleteUser, getUserActive, getUserByUserName, updateUser } from "../../models/users-model/use-model.service";


export const createUserService = async(payload:UserDtoCreate):Promise<InUser>=>{

    payload.password = encodeBcrypt(payload.password);
    return await createUser(payload);
}



export const getUserByUserNameService = async(userName:string, password:string):Promise<string | JwtPayload>=>{
    const user =  await getUserByUserName(userName);
    if(!user) throw Error('User not found. Please verify and try again')
    if(!validateBcrypt(password, user.password)) throw Error('Credentials invalid. Please verify and try again');

    return encodeToken(user.id)

}


export const activeUserService  = async(payload:InUser):Promise<string | InUser>=>{
    if(payload.isActive) throw Error('User is active');

    return await activeUser(payload.id);
}


export const deleteUserService =  async(id:string):Promise<Boolean>=>{
    return await deleteUser(id);
}


export const getUserActiveService = async(id:string):Promise<InUser>=>{
    return await getUserActive(id);
}

export const updateUserService = async(id:string, payload:UserDtoUpdate):Promise<InUser>=>{
    return await updateUser(id, payload);
}


