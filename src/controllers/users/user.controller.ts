import { Request, Response } from "express";
import {  activeUserService, createUserService,  deleteUserService, getUserActiveService, getUserByUserNameService, updateUserService } from "./user.service";
import { validationReqBody } from "../handleError/handleError";

export const createUserController = async(req:Request, res:Response):Promise<Response>=>{

    await validationReqBody(req, res);

    
    return res.status(201).json({
        status:201,
        data:await createUserService(req.body)
    })
}


export const getSessionUserController = async (req:Request, res:Response):Promise<Response>=>{
    try {

        await validationReqBody(req, res);

        return res.status(200).json({
            status:200,
            token:await getUserByUserNameService(req.body['userName'], req.body['password'])
        })
        
    } catch ({message}) {
        return res.status(404).json({
            status:404,
            msg:message
        })
    }
}

export const activeUserController = async (req:Request, res:Response):Promise<Response>=>{
   try {
    return res.status(200).json({
        status:200,
        data:await activeUserService(res.locals.user)
    })
   } catch ({message}) {
        return res.status(403).json({
            status:403,
            data: message
        })
   }
}


export const deleteUserController = async (req:Request, res:Response):Promise<Response>=>{
    return res.status(200).json({
            status:200,
            data:await deleteUserService(res.locals.user.id)
        })
}

export const getUserActiveController = async (req:Request, res:Response):Promise<Response>=>{
    return res.status(200).json({
            status:200,
            data:await getUserActiveService(res.locals.user.id)
        })
}


export const updateUserController =async (req:Request, res:Response):Promise<Response>=>{

    await validationReqBody(req, res);
    return res.status(200).json({
            status:200,
            data:await updateUserService(res.locals.user.id, req.body)
        })
}



