import { Request, Response } from "express";
import { messageMQTTService } from "./messaage.service";



export const sendMessageMQTT = async(req:Request, res:Response):Promise<Response>=>{
    return res.status(200).json({
        status:200,
        data:await messageMQTTService(req.body.message, res.locals.user.id)
    })
}