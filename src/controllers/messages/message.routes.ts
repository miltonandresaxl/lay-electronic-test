import { Router } from "express";
import { authMiddleware } from "../../middleware/authMiddleware";
import { sendMessageMQTT } from "./message.controller";

const _routerMessage = Router()


/**
 * 
 * @swagger
 * 
 * /messages/send:
 * 
 *      post:
 *          summary: Returns send message mqtt
 *          security:
 *             - bearerAuth: []
 *          requestBody:
 *              description: send message mqtt
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 * 
 *                          properties:
 *                              message:
 *                                  type: string
 *                          required:
 *                              - message
 * 
 *          responses:
 *              200:
 *                  description: send message mqtt
 *                  content:
 *                      application/json:
 *                          schema:
 *                              type: object
 *                              
 *              400:
 *                  description: Bad request
 *                  content:
 *                      application/json:
 *                          schema:
 *                              $ref: '#/components/schemas/Error'
 *              403:
 *                  description: Forbidden
 *                  content:
 *                      application/json:
 *                          schema:
 *                              $ref: '#/components/schemas/Forbidden'
 *                  
 *          
 * 
 */

_routerMessage.post('/send', [authMiddleware], sendMessageMQTT)


export default _routerMessage;