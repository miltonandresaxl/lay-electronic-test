import { connectionAndSendMessage } from "../../config/MQTT"



export const messageMQTTService = async(message:string, user_id:string):Promise<any>=>{

 
    return await connectionAndSendMessage(message, user_id);
}