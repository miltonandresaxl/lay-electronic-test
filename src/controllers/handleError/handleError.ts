import { Request, Response } from "express";
import { validationResult } from "express-validator";
import { getUserByUserName } from "../../models/users-model/use-model.service";

export const validationReqBody = async(req:Request, res:Response)=>{
    const errors  = validationResult(req);
    
    const response = (res:Response, msg:string | [] | any )=>  res.status(400).json({
        status:400,
        msg
    })


    if(!errors.isEmpty())
        return response(res, errors);

  

    if(req.originalUrl === '/users/update')
        return;
        
    
    const user = await getUserByUserName(req.body['userName']);
    if(req.originalUrl === '/users' && user)
        return response(res, 'userName in use');

   

    if( !/^[a-zA-Z0-9_]*$/.test(req.body['userName'].toLowerCase()) )
        return response(res, 'userName have spaces');
    
}