import { Request, Response, Router } from "express"; 
import _router from "../controllers/users/user.routes";
import _routerToken from "../controllers/tokens/token.routes";
import _routerMessage from "../controllers/messages/message.routes";

const index = Router();




index.use('/users', _router);

index.use('/authorization', _routerToken);

index.use('/messages', _routerMessage)

index.use('*', (req:Request, res:Response)=>{
    return res.status(404).json({
        status:404,
        message:"Route no found. Please verify and try again."
    });
})

export default index;