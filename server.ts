import app from "./src/app";
import dotenv from './src/config/enviroments';

app.listen(dotenv.port, ()=> console.log(`On port localhost:${dotenv.port}`))