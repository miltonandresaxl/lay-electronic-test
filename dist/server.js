"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = __importDefault(require("./src/app"));
var enviroments_1 = __importDefault(require("./src/config/enviroments"));
app_1.default.listen(enviroments_1.default.port, function () { return console.log("On port localhost:" + enviroments_1.default.port); });
