"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var dotenv_1 = __importDefault(require("dotenv"));
var dotenvLoad = function () {
    var envPath = 'src/env/.env';
    dotenv_1.default.config({ path: envPath });
    return {
        port: process.env.port,
        db_url: "mongodb+srv://" + process.env.db_user + ":" + process.env.db_password + "@" + process.env.db_url + "/" + process.env.db_database + "?retryWrites=true&w=majority",
        tokenSALT: process.env.tokenSALT || '',
        bcryptSALT: process.env.bcryptSALT || '',
        mqtt_url: "mqtt://" + process.env.mqtt_url + ":" + process.env.mqtt_port,
        mqtt_topic: "" + process.env.mqtt_topic
    };
};
exports.default = dotenvLoad();
