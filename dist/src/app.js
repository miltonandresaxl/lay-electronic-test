"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var cors_1 = __importDefault(require("cors"));
var morgan_1 = __importDefault(require("morgan"));
var swagger_ui_express_1 = __importDefault(require("swagger-ui-express"));
var swagger_jsdoc_1 = __importDefault(require("swagger-jsdoc"));
var enviroments_1 = __importDefault(require("./config/enviroments"));
var connectionMongo_1 = __importDefault(require("./db/connectionMongo"));
var index_routes_1 = __importDefault(require("./routes/index.routes"));
var swagger_1 = require("./config/swagger");
//Initializer
var app = express_1.default();
enviroments_1.default;
connectionMongo_1.default;
//middleware
app
    .use(cors_1.default())
    .use(morgan_1.default('dev'))
    .use(express_1.default.json())
    //routes 
    .use(index_routes_1.default)
    //swagger 
    .use('/api/doc', swagger_ui_express_1.default.serve, swagger_ui_express_1.default.setup(swagger_jsdoc_1.default(swagger_1.options)));
exports.default = app;
